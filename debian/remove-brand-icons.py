#!/usr/bin/python3

"""Identify all brand icons in meta.json and remove them.

This produces a new meta.json with the brand icons removed, and also a list
of excluded icons.
"""

import json
from typing import Any


def is_dfsg(icon: dict[str, Any]) -> bool:
    return "Brand / Logo" not in icon["tags"]


with open("meta.json") as meta:
    icons = json.load(meta)

icons_dfsg = []
excluded = []

for icon in icons:
    if is_dfsg(icon):
        icons_dfsg.append(icon)
    else:
        excluded.append(icon["name"])

with open("meta-dfsg.json", "w") as meta_dfsg:
    json.dump(icons_dfsg, meta_dfsg, separators=(",", ":"))

with open("excluded.list", "w") as excluded_file:
    for name in excluded:
        print(name, file=excluded_file)
