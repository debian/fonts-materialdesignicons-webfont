#!/usr/bin/python3

"""Pretty-print the meta.json file."""

import json
import sys

metafile = "meta.json"
if len(sys.argv) > 1:
    metafile = sys.argv[1]

with open(metafile) as meta:
    icons = json.load(meta)

print(json.dumps(icons, indent=4))
